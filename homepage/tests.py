from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from selenium import webdriver
from django.urls import resolve
from django.apps import apps
from homepage.apps import HomepageConfig
from .forms import *
from .views import *
from .models import *
import unittest
import time

# Create your tests here.
class unitTest(TestCase):
    def test_index_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_confirmation_url_exist(self):
        response = Client().get('/confirmation/')
        self.assertEqual(response.status_code,200)

#====================================================================================

    def test_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_confirmation_template(self):
        response = Client().get('/confirmation/')
        self.assertTemplateUsed(response, 'confirmation.html')

#====================================================================================

    def test_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_function_confirmation(self):
        found = resolve('/confirmation/')
        self.assertEqual(found.func, confirmation)

#====================================================================================

    def test_create_new_status(self):
        new = Status.objects.create(name = 'Me', status = 'Good')
        self.assertTrue(isinstance(new, Status))
        self.assertTrue(new.__str__(), new.name)
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_display_status(self):
        status = "Good"
        data = {'stat' : status}
        post_data = Client().post('/', data)
        self.assertEqual(post_data.status_code, 200)

#====================================================================================
    
    def test_index_form_valid(self):
        response = self.client.post('', data={'name':'Me', 'status':'Gud'})
        response_content = response.content.decode()
        self.assertNotIn(response_content, 'Me')
        self.assertNotIn(response_content, 'Gud')

    def test_confirmation_form_valid(self):
        response = self.client.post('/confirmation/', data={'name':'Me', 'status':'Gud'})
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)
        self.assertEqual(response.status_code, 302)
        response_content = response.content.decode()
        self.assertIn(response_content, 'Me')
        self.assertIn(response_content, 'Gud')

#====================================================================================

class functionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        self.driver.quit()

    def test_submit_yes_story7(self):
        self.driver.get(self.live_server_url)
        time.sleep(3)
        name_box = self.driver.find_element_by_name('name')
        name_box.send_keys('Aku')
        status_box = self.driver.find_element_by_name('status')
        status_box.send_keys('Hebat')
        submit_button = self.driver.find_element_by_name('submit')
        submit_button.click()
        time.sleep(3)
        save_button = self.driver.find_element_by_name('save')
        save_button.click()
        time.sleep(3)
        self.assertIn('Aku', self.driver.page_source)
        self.assertIn('Hebat', self.driver.page_source)

    def test_submit_no_story7(self):
        self.driver.get(self.live_server_url)
        time.sleep(3)
        name_box = self.driver.find_element_by_name('name')
        name_box.send_keys('Siapa')
        status_box = self.driver.find_element_by_name('status')
        status_box.send_keys('Not Gud')
        submit_button = self.driver.find_element_by_name('submit')
        submit_button.click()
        time.sleep(3)
        save_button = self.driver.find_element_by_name('back')
        save_button.click()
        time.sleep(3)
        self.assertNotIn('Siapa', self.driver.page_source)
        self.assertNotIn('Not Gud', self.driver.page_source)