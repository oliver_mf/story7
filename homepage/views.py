from django.shortcuts import render
from django.shortcuts import redirect
from .models import Status
from .forms import StatusForm

# Create your views here.
def index(request):
    status = Status.objects.all()
    if (request.method == 'POST'):
        form = StatusForm(request.POST)
        if (form.is_valid()):
            new = Status()
            new.name = form.cleaned_data['name']
            new.status = form.cleaned_data['status']    
            response = {'name':new.name, 'status':new.status}
            return render(request, 'confirmation.html', response)
    else:
        form = StatusForm()
            
    response = {'status':status, 'form':form}
    return render(request, 'index.html', response)

def confirmation(request):
    if (request.method == 'POST'):
        form = StatusForm(request.POST)
        if (form.is_valid()):
            new = Status()
            new.name = form.cleaned_data['name']
            new.status = form.cleaned_data['status']
            new.save()
            return redirect('/')
    else:
        form = StatusForm()

    response = {'form':form}
    return render(request, 'confirmation.html', response)

